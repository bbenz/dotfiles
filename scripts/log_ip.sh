#!/usr/bin/env bash
LOG=/var/log/ip.log
touch $LOG
most_recent_logged_ip=$(tail -n 1 $LOG)
echo $most_recent_logged_ip
current_ip=$(curl -s ifconfig.me)
echo $current_ip
if [[ $most_recent_logged_ip == $current_ip ]]
then
  date >> /tmp/ip_log
  echo $current_ip >> /tmp/ip_log
else
  echo "" >> $LOG
  date >> $LOG
  echo $current_ip >> $LOG
fi


