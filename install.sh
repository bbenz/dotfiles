#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m'

#bash
if [ -f "$HOME/.bashrc" ]
then
  if [ -L "$HOME/.bashrc" ]; then
    echo -e "${BLUE}bashrc was already linked${NC}"
  else
    echo -e "${RED}a bash rc file already exists and it's not the one linked from your dotfiles${NC}"
  fi
else
  ln -s $(pwd)/bashrc ~/.bashrc
  echo -e "${GREEN}bashrc has been linked${NC}"
fi

# vim
if [ -d "$HOME/.vim" ]
then
  if [ -L "$HOME/.vim" ]; then
    echo -e "${BLUE}vim was already linked${NC}"
  else
    echo -e "${RED}a vim dir already exists and it's not the one linked from your dotfiles${NC}"
  fi
else
  ln -s $(pwd)/vim ~/.vim
  echo -e "${GREEN}vim has been linked${NC}"
fi

# git
if [ -f "$HOME/.gitconfig" ]
then
  if [ -L "$HOME/.gitconfig" ]; then
    echo -e "${BLUE}gitconfig was already linked${NC}"
  else
    echo -e "${RED}a gitconfig already exists and it's not the one linked from your dotfiles${NC}"
  fi
else
  ln -s $(pwd)/gitconfig ~/.gitconfig
  echo -e "${GREEN}git has been linked${NC}"
fi

# tmux
if [ -f "$HOME/.tmux.conf" ]
then
  if [ -L "$HOME/.tmux.conf" ]; then
    echo -e "${BLUE}tmux was already linked${NC}"
  else
    echo -e "${RED}a tmux file already exists and it's not the one linked from your dotfiles${NC}"
  fi
else
  ln -s $(pwd)/tmux.conf ~/.tmux.conf
  echo -e "${GREEN}tmux has been linked${NC}"
fi

# nvim
NVIM_CONFIG_DIR="$HOME/.config/nvim"
if [ -d "$NVIM_CONFIG_DIR" ]
then
  if [ -L "$NVIM_CONFIG_DIR" ]; then
    echo -e "${BLUE}nvim config was already linked${NC}"
  else
    echo -e "${RED}an nvim config dir already exists and it's not the one linked from your dotfiles${NC}"
  fi
else
  mkdir -p "$NVIM_CONFIG_DIR"
  ln -s $(pwd)/nvim/init.lua $NVIM_CONFIG_DIR/init.lua
  echo -e "${GREEN}nvim config has been linked${NC}"
fi
