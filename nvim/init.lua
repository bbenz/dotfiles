-- :PackerInstall
-- :PackerClean
-- Automatically install packer.nvim if not installed
-- disable netrw at the very start of your init.lua, this is advised by nvim-tree
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

local install_path = vim.fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  vim.cmd('!git clone https://github.com/wbthomason/packer.nvim '..install_path)
end
-- if this next line gives you trouble you may have to manually run `:TSInstall vim`, even if treesitter reports that it's been installed
vim.cmd [[packadd packer.nvim]]

require('packer').startup(function()
  -- don't put any setup configs in here it doesn't work
  use 'wbthomason/packer.nvim'
  use 'tpope/vim-fugitive'
  use 'tpope/vim-commentary'
  use { 'rose-pine/neovim', as = 'rose-pine', }
  use { 'rmagatti/auto-session', }
  use { 'joshdick/onedark.vim' } 
  use { "williamboman/mason.nvim" }
  use { "akinsho/toggleterm.nvim" }
  use { "lewis6991/gitsigns.nvim" }
  use { "nvim-tree/nvim-web-devicons" }
  use { "nvim-tree/nvim-tree.lua" }
  use { 'nvim-telescope/telescope.nvim', tag = '0.1.2', requires = { {'nvim-lua/plenary.nvim'} } }
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
  use { 'akinsho/bufferline.nvim', tag = "*", requires = 'nvim-tree/nvim-web-devicons' }
  use { 'nvim-lualine/lualine.nvim', requires = { 'nvim-tree/nvim-web-devicons', opt = true } }
  use {
    "folke/which-key.nvim",
    config = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300 -- I don't think this is doing anything
    end
  }
end)

vim.opt.mouse = 'a'
vim.o.mousemoveevent = true

-- :Mason, :MasonUpdate
require("mason").setup()
require("nvim-tree").setup(
  {
    filters = {
      git_ignored = false,
    }
  }
)
require("toggleterm").setup{
  open_mapping = [[<c-/>]],
  direction = 'float',
}
require("auto-session").setup {
  log_level = "error",
  auto_session_allowed_dirs = { "~/Projects/*", },
  auto_session_root_dir = "~/.config/nvim/sessions/",
  auto_session_enabled = true,
  auto_save_enabled = true,
  auto_session_use_git_branch = true,
}
require('gitsigns').setup{}
require("which-key").setup{
  plugins = {
    presets = {
      operators = false, -- adds help for operators like d, y, ...
      motions = false, -- adds help for motions
      text_objects = false, -- help for text objects triggered after entering an operator
      windows = true, -- default bindings on <c-w>
      nav = true, -- misc bindings to work with windows
      z = true, -- bindings for folds, spelling and others prefixed with z
      g = true, -- bindings for prefixed with g
    },
  },
}
local bufferline = require('bufferline')
bufferline.setup{
  options = {
    numbers = "ordinal",
    style_preset = {
        bufferline.style_preset.no_italic,
        bufferline.style_preset.no_bold,
        bufferline.style_preset.minimal,
    },
    hover = {
        enabled = true,
        delay = 200,
        reveal = {'close'}
    },
    offsets = {
        {
            filetype = "NvimTree",
            text = "File Explorer",
            text_align = "left",
            separator = true
        }
    },
  }
}
require("rose-pine").setup({ 
  dark_variant = "moon",
  disable_italics = true,
})

require('lualine').setup()


-- :TSInstallInfo
require('nvim-treesitter.configs').setup {
  ensure_installed = { 
    "bash",
    "c",
    "javascript",
    "json",
    "lua",
    "python",
    "typescript",
    "vue",
    "tsx",
    "css",
    "rust",
    "yaml",
  },
  highlight = {
    enable = true,
  }
}

vim.g.mapleader = ' '

-- some vim basics
vim.opt.number = true
vim.opt.hlsearch = true
vim.opt.wrap = false
vim.opt.equalalways = true -- splits open equal size
vim.opt.splitbelow = true -- as opposed to split above
vim.opt.splitright = true -- only works on vert splits
vim.opt.autoindent = true
vim.opt.timeoutlen = 1000
vim.opt.ttimeoutlen = 50 -- fix the annoying delay on esc
vim.opt.ignorecase = true -- prefix searches with \C to set them as case-sensitive
vim.opt.startofline = false
vim.opt.signcolumn = "yes" -- add two spaces to left of buffer, keeps gitsigns from shifting layout when git info appears
vim.o.autoindent = true -- newline behavior, newlines using normal mode "o" should indent based on surrounding text context


-- set tab behavior
vim.opt.tabstop = 2 -- tab characters will look 2 spaces wide
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.shiftround = true -- when you tab and >> and <C-T> it will automatically round out to 2

vim.cmd('colorscheme rose-pine')
vim.cmd('nohlsearch') -- reloading this config file was triggering a highlight search, this turns that off

-- require('onedark').setup {
--     style = 'darker'
-- }
-- require('onedark').load()

local map = vim.api.nvim_set_keymap
local options = { noremap = true }

-- capital Y will copy to clipboard in normal and visual mode
map('n', 'Y', '"+y', {noremap = true, silent = true})
map('v', 'Y', '"+y', {noremap = true, silent = true})


-- Map control-backspace to control-w in insert mode
map('i', '<C-BS>', '<C-w>', {})

-- Move line behavior
map('n', '<C-j>', ':m .+1<CR>==', options)
map('n', '<C-k>', ':m .-2<CR>==', options)
map('i', '<C-j>', '<Esc>:m .+1<CR>==', options)
map('i', '<C-k>', '<Esc>:m .-2<CR>==', options)
map('v', '<C-j>', ":m '>+1<CR>gv=gv", options)
map('v', '<C-k>', ":m '<-2<CR>gv=gv", options)

-- add labels to which-key for move line and system clipboard behavior
require("which-key").register({
  ["<C-j>"] = { "Move line down" },
  ["<C-k>"] = { "Move line up" },
  ["Y"] = { "yank to system clipboard" },
})


-- jump to end of line in insert mode
map('i', '<C-e>', '<C-o>A', options)
map('i', '<C-h>', '<C-o>I', options)

-- Open help files in vertical split
vim.api.nvim_exec([[
  autocmd FileType help wincmd L
]], false)

-- cursor should pick up where I last lest off for a given file
vim.cmd [[
  autocmd BufReadPost * if line("'\"") >= 1 && line("'\"") <= line("$") | execute "normal! g`\"" | endif
]]


-- this maps <leader>number to bufferline
for i = 1, 9 do
  map("n", "<leader>" .. i,
    "<Cmd>lua require('bufferline').go_to_buffer(" .. i .. ", true)<cr>", 
    { noremap = true, silent = true }
  )
end

vim.cmd [[
  command! NvimTreeToggleNoFocus :let winid=winnr() | NvimTreeToggle | exe winid . "wincmd w"
]]



-- all leader prefix codes that will go in which-key
require("which-key").register({
  o = { "<cmd>Telescope find_files<cr>", "open file" },
  c = { "<cmd>bw<cr>", "close buffer" },
  e = { "<cmd>NvimTreeToggleNoFocus<cr>", "File Explorer" },
  h = { "<cmd>nohl<cr>", "no highlight" },
  v = { "<cmd>e ~/.config/nvim/init.lua<cr>", "open ~/.config/nvim/init.lua" },
  x = { "<cmd>let @+=expand('%:~:.')<CR>", "copy filepath" },
  m = { "<cmd>Mason<cr>", "Mason dashboard" },
  w = { "<cmd>WhichKey<cr>", "WhichKey main page" },
  -- l = { name = "+treesitter" },
  s = { "<cmd>Telescope treesitter<cr>", "treesitter search" },
  b = { "<cmd>Telescope buffers<cr>", "buffers" },
  p = { "<cmd>Telescope oldfiles<cr>", "previously opened files" },
  ["/"] = { '<cmd>Telescope current_buffer_fuzzy_find<CR>', "fuzzy find current buffer" },
  r = { 
    name = "+ripgrep, reload, relativenumber",
    l = { '<cmd>luafile ~/.config/nvim/init.lua<CR>:echo "init.lua reloaded"<CR>', "reload" },
    n = { '<cmd>setlocal relativenumber!<CR>', "relativenumber" },
    g = { '<cmd>Telescope live_grep<CR>', "ripgrep" },
  },
  -- git related
  g = {
    name = "+git", -- The name for the <leader>g prefix
    c = { "<cmd>Telescope git_bcommits<cr>", "commits for this buffer" },
    j = { "<cmd>lua require 'gitsigns'.next_hunk({navigation_message = false})<cr>", "Next Hunk" },
    k = { "<cmd>lua require 'gitsigns'.prev_hunk({navigation_message = false})<cr>", "Prev Hunk" },
    t = { "<cmd>lua require 'gitsigns'.toggle_current_line_blame<cr>", "Toggle line blame" },
    f = { "<cmd>Telescope git_files<cr>", "open file (from git repo)" },
    a = { "<cmd>Telescope git_commits<cr>", "all commits" },
    s = { "<cmd>Telescope git_status<cr>", "git status" },
    p = { "<cmd>Telescope git_stash<cr>", "list stashes & pop them" },
    b = { "<cmd>Git blame<cr>", "git blame" },
    l = { "<cmd>vertical Git log<cr>", "git log" },
    g = { "<cmd>vertical Git<cr>", "Tpope's git dashboard" },
  },
  -- telescope functions
  t = {
    name = "+telescope", -- The name for the <leader>t prefix
    a = { "<cmd>Telescope builtin<cr>", "all builtin telescope pickers" },
    s = { '<cmd>Telescope grep_string<CR>', "search pwd for string under cursor" },
    f = { "<cmd>Telescope find_files<cr>", "find files (from pwd)" },
    m = { "<cmd>Telescope marks<cr>", "marks" },
    r = { "<cmd>Telescope registers<cr>", "registers" },
    k = { "<cmd>Telescope keymaps<cr>", "keymaps" },
    c = { "<cmd>Telescope colorscheme<cr>", "colorscheme" },
    p = { "<cmd>lua require('telescope.builtin').colorscheme({enable_preview = true})<cr>", "colorscheme with preview", },
    C = { "<cmd>Telescope commands<cr>", "plugin Commands" },
    j = { "<cmd>Telescope jumplist<cr>", "jumplist" },
    h = { '<cmd>Telescope command_history<CR>', "command history" },
  },
  -- treesitter
  T = {
    name = "+treesitter",
    h = { "<cmd>help nvim-treesitter<CR>", "help" },
    i = { "<cmd>TSInstallInfo<CR>", "info" },
    I = { "<cmd>TSInstall<CR>", "install all parsers in ensure_installed" },
    u = { "<cmd>TSUpdate<CR>", "update all installed parsers" },
  },
  -- packer
  P = {
    name = "+packer",
    i = { "<cmd>PackerInstall<CR>", "install - Clean, then install missing plugins" },
    c = { "<cmd>PackerClean<CR>", "clean - Remove any disabled or unused plugins" },
    s = { "<cmd>PackerStatus<CR>", "status - Show list of installed plugins" },
    u = { "<cmd>PackerUpdate<CR>", "update - Clean, then update and install plugins" },
    y = { "<cmd>PackerSync<CR>", "sync - Perform `PackerUpdate` and then `PackerCompile`" },
  },
  -- ignore numbers 1-9
  ["1"] = "which_key_ignore",
  ["2"] = "which_key_ignore",
  ["3"] = "which_key_ignore",
  ["4"] = "which_key_ignore",
  ["5"] = "which_key_ignore",
  ["6"] = "which_key_ignore",
  ["7"] = "which_key_ignore",
  ["8"] = "which_key_ignore",
  ["9"] = "which_key_ignore",
}, { prefix = "<leader>" })

