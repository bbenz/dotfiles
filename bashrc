#!/usr/bin/env bash

# dotfiles dir is "dotfiles" in version control, remind to rename if it's not renamed
if [ ! -d "$HOME/.dotfiles" ]; then
  red_strong="\e[1;31m";
  reset="\e[0m";
  echo -e "${red_strong}rename dotfiles to .dotfiles!"
fi

# setup colors
reset_color="\e[0m";
black="\e[0;30m";
blue="\e[0;34m";
cyan="\e[0;36m";
green="\e[0;32m";
orange="\e[0;33m";
purple="\e[0;35m";
red="\e[0;31m";
violet="\e[0;35m";
white="\e[0;37m";
yellow="\e[0;33m";

black_strong="\e[1;30m";
blue_strong="\e[1;34m";
cyan_strong="\e[1;36m";
green_strong="\e[1;32m";
orange_strong="\e[1;33m";
purple_strong="\e[1;35m";
red_strong="\e[1;31m";
violet_strong="\e[1;35m";
white_strong="\e[1;37m";
yellow_strong="\e[1;33m";

# add things to path
export PATH="~/.composer/vendor/bin:$PATH"
export PATH="~/.local/bin:$PATH"

# prompt
# Shell prompt based on the Solarized Dark theme.
# Screenshot: http://i.imgur.com/EkEtphC.png
# Heavily inspired by @necolas’s prompt: https://github.com/necolas/dotfiles
# iTerm → Profiles → Text → use 13pt Monaco with 1.1 vertical spacing.

prompt_git() {
	local s='';
	local branchName='';

	# Check if the current directory is in a Git repository.
	git rev-parse --is-inside-work-tree &>/dev/null || return;

	# Check for what branch we’re on.
	# Get the short symbolic ref. If HEAD isn’t a symbolic ref, get a
	# tracking remote branch or tag. Otherwise, get the
	# short SHA for the latest commit, or give up.
	branchName="$(git symbolic-ref --quiet --short HEAD 2> /dev/null || \
		git describe --all --exact-match HEAD 2> /dev/null || \
		git rev-parse --short HEAD 2> /dev/null || \
		echo '(unknown)')";

	# Early exit for Chromium & Blink repo, as the dirty check takes too long.
	# Thanks, @paulirish!
	# https://github.com/paulirish/dotfiles/blob/dd33151f/.bash_prompt#L110-L123
	repoUrl="$(git config --get remote.origin.url)";
	if grep -q 'chromium/src.git' <<< "${repoUrl}"; then
		s+='*';
	else
		# Check for uncommitted changes in the index.
		if ! $(git diff --quiet --ignore-submodules --cached); then
			s+='+';
		fi;
		# Check for unstaged changes.
		if ! $(git diff-files --quiet --ignore-submodules --); then
			s+='!';
		fi;
		# Check for untracked files.
		if [ -n "$(git ls-files --others --exclude-standard)" ]; then
			s+='?';
		fi;
		# Check for stashed files.
		if $(git rev-parse --verify refs/stash &>/dev/null); then
			s+='$';
		fi;
	fi;

	[ -n "${s}" ] && s=" [${s}]";

	echo -e "${1}${branchName}${2}${s}";
}


if tput setaf 1 &> /dev/null; then
	tput sgr0; # reset colors
	bold=$(tput bold);
	reset=$(tput sgr0);
	# Solarized colors, taken from http://git.io/solarized-colors.
	black=$(tput setaf 0);
	blue=$(tput setaf 33);
	cyan=$(tput setaf 37);
	green=$(tput setaf 64);
	bright_green=$(tput setaf 2);
	orange=$(tput setaf 166);
	bright_orange=$(tput setaf 208);
	purple=$(tput setaf 125);
	pink=$(tput setaf 201);
	red=$(tput setaf 124);
	violet=$(tput setaf 61);
	white=$(tput setaf 15);
	yellow=$(tput setaf 136);
else
	bold='';
	reset="\e[0m";
	black="\e[1;30m";
	blue="\e[1;34m";
	cyan="\e[1;36m";
	green="\e[1;32m";
	orange="\e[1;33m";
	purple="\e[1;35m";
	red="\e[1;31m";
	violet="\e[1;35m";
	white="\e[1;37m";
	yellow="\e[1;33m";
fi;

# default blue and pink
if tput setaf 1 &> /dev/null; then
  username_color="$(tput setaf 33)"
  host_color="$(tput setaf 201)"
fi
 
# Set the terminal title and prompt.
PS1="${debian_chroot:+($debian_chroot)}"
PS1+="\[${bold}\]";
PS1+="\[${username_color}\]\u"; # username
PS1+="\[${host_color}\]@\h"; # host
PS1+="\[${reset}\]:\[${bold}\]\w";
PS1+="\$(prompt_git \"\[${white}\] on \[${red}\]\" \"\[${blue}\]\")";
PS1+="\[${white}\]\$ \[${reset}\]"; # `$` (and reset color)
PS1+="\[${reset}\]"; # `$` (and reset color)
# PS1+="\n";
PS1+="\[\e]0;\u@\h: \w\a\]" # add location information to terminal window title bar
export PS1;

PS2="\[${bold}\]\[${blue}\]→ \[${reset}\]";
export PS2;


# exports
export VISUAL=vim
export EDITOR="$VISUAL"

# Eternal bash history.
# ---------------------
# Undocumented feature which sets the size to "unlimited".
# https://stackoverflow.com/questions/9457233/unlimited-bash-history
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_eternal_history
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"


# NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# aliases
alias lsd='ls -ldh -- */'
alias lsf='ls -alph --color=always | grep -v /'
alias lsdd='ls -ldh -- .*/'
alias lsdf='ls -aldph .* --color=always | grep -v /'

alias l='ls -l'
alias ll='ls -al'
alias dotfiles='cd ~/.dotfiles'
alias dot='cd ~/.dotfiles'

alias grep='grep --color=always'
alias fgrep='fgrep --color=always'
alias egrep='egrep --color=always'

# docker stuff
alias dc="docker-compose"
alias dcr="docker-compose run --rm"
alias dcupd="docker-compose up -d"
alias dcdown="docker-compose down"

# git stuff
alias vimgit="vim +Git +only"
alias main='git checkout main'
alias qa='git checkout qa'
alias gs='~/.dotfiles/gs_git_status.sh'
alias gsl='git stash list'
alias gsp='git stash push'
alias gspm='git stash push --message'
alias gls='git log -S' # pickaxe, search a string's changes in the repo over time https://git-scm.com/book/en/v2/Git-Tools-Searching
alias ga='git add . && git status -s'
alias gr='git reset -q && git status -s'
alias gc='git checkout'
alias gb='git branch'
alias gcb='git checkout -b '
alias gcm='git commit -m'
alias gp='git pull'
alias gpp='git push' # mnemonic: you have to pull before you can push, right?
alias gm='git merge'

# Print each PATH entry on a separate line
alias path='echo -e ${PATH//:/\\n}'
alias xmod='/usr/bin/time -v xmodmap ~/.Xmodmap'

# show free memory
alias mem='free -m | grep Mem: | awk '\''{print $4 }'\'''
# show file sizes
alias dush='du -sh * | sort -h'
# information about operating system
alias os='lsb_release -a && screenfetch'

alias disk='baobab'

alias vimgit='vim +Git +only'


# functions
# color man pages
function _colorman() {
  env \
    LESS_TERMCAP_mb=$(printf "\e[1;35m") \
    LESS_TERMCAP_md=$(printf "\e[1;34m") \
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_se=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[7;40m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[1;33m") \
      "$@"
}
function man { _colorman man "$@"; }

# print colors
function colors {
  color_output=$(tput bold)
  for R in $(seq 0 255); do
    color=$(tput setaf ${R})
    color_output+="$(tput bold)${color}color number ${R}\n";
  done
  echo -e "$color_output" | less -R
}

# tree defaults
function tre {
  # -F --quit-if-one-screen
  # -R --RAW-CONTROL-CHARS (sends ASNI color escapes sequences. allows colors)
  # -N --LINE-NUMBERS
  # -X --no-init don't clear the screen before printing tree
	tree -aC -I '.git|node_modules|bower_components|__pycache__|admin' --dirsfirst "$@" | less -FRNX;
}

# make dir and cd into it, recursive works. -p means parent
function mkdircd {
  command mkdir -p $1 && cd $1
}

# grep-less defaults
function grepl {
  grep --color=always "$@" | less -FRXN
}

# list functions in this file
function functions {
  thisfile="$HOME/.dotfiles/bash/functions"
  cat $thisfile | grep '^function.*{' |  awk '{print $2}' | grep -v '^_'
  # cat $0 | grep 'function'
}

# show ip address
function ipx {
	router_color=$(tput setaf 45);
	global_color=$(tput setaf 46);
  echo -e "${host_color}$(hostname): ${white}$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')"
  echo -e "${router_color}router: ${white}$(netstat -nr | awk '$1 == "0.0.0.0"{print$2}')"
  echo -e "${global_color}global: ${white}$(curl -s ifconfig.me)${reset}"
}

function psauxgrep {
  ps -aux | grep $@ | awk '{print $2 "\t" $11 "\t" $12}' | grep -v 'grep'
}

# sensible shopts
# histappend - append to history file instead of overwriting it
shopt -s histappend
# checkwinsize - update the value of LINES and COLUMNS after each command if altered
shopt -s checkwinsize

# set screen prompt
if [ -n "$STY" ]; then export PS1="(screen) $PS1"; fi


# this block is for interactive shell
if [[ $- == *i* ]] ; then 
  # bash style ctrl+w erase words
  # stty werase undef
  # stty werase '^H'
  # ^H will now erase words
  bind '\C-h:unix-filename-rubout'
  # bind '\C-w:unix-filename-rubout'
  cd ~/.dotfiles
  git status -sb
  cd -
fi


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export WORKON_HOME=~/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh


# speed up zsh load, alias nvm so it only loads when you use it https://superuser.com/a/1611283
export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
alias nvm="unalias nvm; [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"; nvm $@"

