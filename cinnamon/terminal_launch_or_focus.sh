#!/bin/bash
#launch or focus this desktop only
win_class="gnome-terminal-server.Gnome-terminal"
launcher="gnome-terminal"

# list desktops
# print list param 1, space, param 2
# if param $2 is *, current desktop id is $1
this_desktop=$(wmctrl -d | awk '{print $1 " " $2}' | grep \* | awk '{print $1}')

# if win_class is already open, focus it
if wmctrl -x -l | grep --perl-regexp ^.*?\\s\\s$this_desktop | grep --quiet "$win_class"; then

  # get list of all windows matching with the class above
  win_list=$(wmctrl -x -l | grep --perl-regexp ^.*?\\s\\s$this_desktop | grep "$win_class" | awk '{print $1}' )

  # get id of the focused window
  active_win_id=$(xprop -root | grep '^_NET_ACTIVE_W' | awk -F'# 0x' '{print $2}')
  if [ "$active_win_id" == "0" ]; then
    active_win_id=""
  fi

  # get next window to focus on, removing id active
  switch_to=$(echo $win_list | sed s/.*$active_win_id// | awk '{print $1}')

  # if the current window is the last in the list ... take the first one
  if [ "$switch_to" == '' ];then
    switch_to=$(echo $win_list | awk '{print $1}')
  fi

  # switch to window
  wmctrl -i -a $switch_to

else
  $launcher
fi
